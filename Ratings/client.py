import requests
import sys
import getpass
import json
from time import sleep


def register():
    username = input("username:")#take username input from user
    password = getpass.getpass("password:")#take password input from user
    email = input("email:")#take email input from user
    data = {'username' : username, 'password': password, 'email' : email} #package as dict
    request = requests.post('ll16m23a.pythonanywhere.com/register/', data = data) #send as post request with data
    print (request.content) #print response

def login():
    username = input("username:")#take username input from user
    password = getpass.getpass("password:")#take password input from user
    data = {'username' : username, 'password': password} #package as dict
    request = requests.post('ll16m23a.pythonanywhere.com/login/', data = data) #send as post request with data
    print (request.content) #print response

def logout():
    request = requests.post('ll16m23a.pythonanywhere.com/logout/') #send post req to logut
    print (request.content) #print response

def listf():
    request = requests.get('ll16m23a.pythonanywhere.com/list/') #get request for list
    reqtype = request.headers.get('content-type') #get header type

    if reqtype == 'application/json': #if header is json
        json = request.json() #parse response as json
        length = len(json['all_info']) #get the length of the json
        print("CODE          NAME                   YEAR            SEMESTER          TAUGHT BY") #header for table
        comparison = 0 #stores char count for longest module name
        spaces = "" #for padding output table
        spaces2 = ""#for padding output table

        for i in range(length):#for each item in json
            module_name = str(json['all_info'][i]['module_name']) #take the module name
            length_module = len(module_name) #take the length of the name
            if (length_module > comparison): #if it has more chars than current biggest name
                comparison = length_module #biggest module length reassigned

        for i in range(length):#for each item in json
            count = str(json['all_info'][i]['count'])#extract relevant data from json
            module_code = str(json['all_info'][i]['module_code'])
            module_name = str(json['all_info'][i]['module_name'])
            module_year = str(json['all_info'][i]['module_year'])
            module_semester = str(json['all_info'][i]['module_semester'])
            length_code = len(module_code) #length of the char in module code
            if (length_code == 2): #if it is only two
                spaces2 = spaces2 + "           " #this padding
            else:
                spaces2 = "          " #otherwise more padding

            if(len(module_name) < comparison):#if the current module name has less char than the most char module name
                difference = abs(comparison - len(module_name)) #find the difference
                for k in range(difference): #for that difference
                    spaces = spaces + " " #apply padding
            spaces = spaces + "     " #default padding for all
            print("--------------------------------------------------------------------------------") #border

            for j in range(int(count)):#for professors in the range of professors teaching that module
                professor_firstname = str(json['all_info'][i]['professor_firstname' + str(j)])#extract professors details
                professor_lastname = str(json['all_info'][i]['professor_lastname' + str(j)])
                professor_id = str(json['all_info'][i]['professor_id' + str(j)])

                flag = 0#default
                if(j > 0):#two or more professor teaching module
                    flag = 1
                    if(flag == 0):#first iteration
                        print(module_code +
                        spaces2  + module_name +
                        spaces + module_year +
                        "                " + module_semester +
                        " " + professor_id + ", Professor" +
                        professor_firstname[0] + "." +
                        professor_lastname)#print all

                    if(flag == 1):#second iteration
                        print("          ""          ""          ""          " +
                        "          ""          ""   " "            "  +
                        professor_id + ", Professor " +
                        professor_firstname[0] + "." +
                        professor_lastname)#print second professor

                else:#else print all of it
                    print(module_code +
                    spaces2 + module_name +
                    spaces + module_year +
                    "                " + module_semester +
                    "          " + professor_id + ", Professor " +
                    professor_firstname[0] + "." +
                    professor_lastname)

            spaces = ""#reset
            spaces2 = ""
        flag = 0
    else:
        print(request.content)
def view():
    request = requests.get('ll16m23a.pythonanywhere.com/view/')#get request
    reqtype = request.headers.get('content-type')#get response header type

    if reqtype == 'application/json': #if json type
        json = request.json() #parse as json
        count = len(json['all_ratings']) #get length of json objects
        for i in range(count): #loop over all objects
            professor_firstname = str(json['all_ratings'][i]['professor_firstname'])#extract relevant info
            professor_lastname = str(json['all_ratings'][i]['professor_lastname'])
            professor_id = str(json['all_ratings'][i]['professor_id'])
            rating = str(json['all_ratings'][i]['rating'])
            professor_firstname1 = professor_firstname[0]

            if (rating == '0'):#convert rating to stars
                stars = ""
            elif (rating == '1'):
                stars = "*"
            elif (rating == '2'):
                stars = "**"
            elif (rating == '3'):
                stars = "***"
            elif (rating == '4'):
                stars = "****"
            elif (rating == '5'):
                stars = "*****"
            #print info
            print('The rating of Professor ' + professor_firstname1 + "." + professor_lastname + "(" + professor_id + ") is " + str(stars))
            print('--------------------------------------------------------------')
    else:
        print(request.content)

def average(option_chosen):
    data = {'professor_id' : option_chosen[1], 'module_code': option_chosen[2]}#take data for average from user and package as dict
    request = requests.post('ll16m23a.pythonanywhere.com/average/', data = data)#send post request with data
    reqtype = request.headers.get('content-type')#get type of header

    if reqtype == 'application/json':#if type is json
        json = request.json()#parse as json
        professor_id = str(json['average_rating']['professor_id'])#extract relevant info from json
        professor_firstname = str(json['average_rating']['professor_firstname'])
        professor_firstname1 = professor_firstname[0]
        professor_lastname = str(json['average_rating']['professor_lastname'])
        module_code = str(json['average_rating']['module_code'])
        module_name = str(json['average_rating']['module_name'])
        average = int(json['average_rating']['average'])

        if (average == 0):#convert rating to stars
            stars = ""
        elif (average == 1):
            stars = "*"
        elif (average == 2):
            stars = "**"
        elif (average == 3):
            stars = "***"
        elif (average == 4):
            stars = "****"
        elif (average == 5):
            stars = "*****"
        #print average
        print("The rating of Professor " + professor_firstname1 + "." + professor_lastname + "(" + professor_id + ") in module " + module_name + "(" + module_code + ") is " + str(stars))
    else:
        print(request.content)

def rate(option_chosen):
    #take data from user and package as dict
    data = {'professor_id' : option_chosen[1], 'module_code': option_chosen[2], 'year': option_chosen[3], 'semester': option_chosen[4], 'rating': option_chosen[5]}
    request = requests.post('ll16m23a.pythonanywhere.com/rate/', data = data) #send post request with data
    print(request.content) #print response



def main():
    while(1):
        #MENU FOR USER HELP
        print("\n \n")
        print("#####################################################")
        print("Choose from the following menu:")
        print("register -- to register as a user.")
        print("login -- to login if you already have an account.")
        print("logout -- to end the session securely.")
        print("list -- to list all modules and the professors teaching them.")
        print("view -- to view the ratings of all professors.")
        print("average -- to view average rating of a certain professor in a certain module.")
        print("rate -- to rate the teaching of a certain professor in a certain module.")
        print("exit -- to exit the program.")
        print("On invalid input a help menu will be shown.")
        print("##################################################### \n \n \n \n")

        option = input('Choose an option from above: ')#ask for choice
        print("\n")
        option_chosen = option.split(" ")#split by space to parse multiple inputs

        if option_chosen[0] == "register":#register chosen, call register functon
            register()
        elif option_chosen[0] == "login":#login chosen, call login function
            login()
        elif option_chosen[0] == "logout":#logout chosen, call logout function
            logout()
        elif option_chosen[0] == "list":#list choen, call list function
            listf()
        elif option_chosen[0] == "view":#view chosen, call view function
            view()
        elif option_chosen[0] == "average":#average chosen
            if len(option_chosen) < 2:#check if professor id and module id been provided by length of the input
                print("Invalid arguments, provie professor id and module id.")#if not error
            else:
                average(option_chosen)#call average function with their input
        elif option_chosen[0] == "rate":#rate chosen
            if len(option_chosen) < 5:#check if info provided by length of the input
                print("Invalid arguments, provide professor id, module id, year, semester and your rating.")#if not error
            else:
                rate(option_chosen)#call rate function with their input
        elif option_chosen[0] == "exit":#exit
            sys.exit(1)
        else:#help
            print("Invalid input.")
            print('HELP MENU:')
            print("register -- no further argument required.")
            print("login -- no further arugment required.")
            print("logout -- no further argument required.")
            print("list -- no further argument required.")
            print("view -- no further argument required.")
            print("average -- provide professor id and module id.")
            print("rate -- provide professor id, module id, year, semester and your rating.")

main()
