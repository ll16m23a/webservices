from django.db import models
import datetime


choice = []
for r in range(2000, (datetime.datetime.now().year+1)):
    choice.append((r,r))
semester_choice = (
(1, "1"),
(2, "2")
)


class Professor(models.Model):
    identifier = models.CharField(max_length = 3, unique = True)
    firstname = models.CharField(max_length = 10)
    lastname = models.CharField(max_length = 10)
    def __str__(self):
        return '%s %s %s' % (self.identifier, self.firstname, self.lastname)

class Module(models.Model):
    m_identifier = models.CharField(max_length = 3, unique = True)
    name = models.CharField(max_length = 50)
    def __str__(self):
        return '%s %s' % (self.m_identifier, self.name)

class Instance(models.Model):
    module = models.ForeignKey(Module, on_delete = models.SET_NULL, null = True)
    professor = models.ManyToManyField('Professor')
    year = models.IntegerField(('year'), choices= choice, default=datetime.datetime.now().year)
    semester = models.IntegerField(choices = semester_choice)
    def __str__(self):
        return '%i %i' % (self.year, self.semester)

class Rating(models.Model):
    professor = models.ForeignKey(Professor, on_delete = models.SET_NULL, null = True)
    module = models.ForeignKey(Instance, on_delete = models.SET_NULL, null = True)
    rating = models.IntegerField()
    def __str__(self):
        return '%i' % (self.rating)
