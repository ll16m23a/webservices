# Generated by Django 3.0.3 on 2020-03-08 18:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Ratings', '0004_auto_20200308_1822'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rating',
            name='module',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='Ratings.Instance'),
        ),
    ]
