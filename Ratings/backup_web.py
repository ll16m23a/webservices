from django.shortcuts import render
from django.http import HttpResponse, HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt
from .models import Professor, Module, Rating
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
import json
from django.core import serializers

flag = 1 #CHANGE ME TO 0

@csrf_exempt
def registerRequest(request):
    bad_response = HttpResponseBadRequest() #prepare bad request
    bad_response['Content-Type'] = 'text/plain' #set to text

    if (request.method != 'POST'): #check if it a post request or not
        bad_response.content = "Only POST requests allowed for this resource." #if its get request throw error
        return bad_response

    username_validation = request.POST['username'] #take username given on command line
    username_search = User.objects.filter(username = username_validation) #check if it already exists
    if username_search: #if it exists already
        bad_response.content = "Username already taken." #already exists so bad request
        return bad_response

    email_validation = request.POST['email']#take email given on command line
    email_search = User.objects.filter(email = email_validation) #check if it already exists
    if email_search: #if it exists
        bad_response.content = "Email already in use with an account." #bad request
        return bad_response
    #otherwise create and register user and return success
    new_user = User.objects.create_user(username = request.POST['username'], password = request.POST['password'], email = request.POST['email'])
    return HttpResponse("User has succesfully registered.")
@csrf_exempt
def loginRequest(request):
    global flag
    bad_response = HttpResponseBadRequest()#setup bad request
    bad_response['Content-Type'] = 'text/plain'#set to text

    if (request.method != 'POST'):#if the request is not post
        bad_response.content = "Only POST requests allowed for this resource."#bad request
        return bad_response
    username = request.POST['username']#take username from command line client
    password = request.POST['password']#take password from command line client
    user = authenticate(username = username, password = password)#authenticate using django
    if user is not None:
        if user.is_active:
            login(request, user)#login using django
            flag = 1
            return HttpResponse("Success")#success
        else:
            bad_response.content = "User is not active."#bad request
            return bad_response
    else:
        bad_response.content = "Username/Password incorrect."#bad request
        return bad_response
@csrf_exempt
def logoutRequest(request):
    global flag
    bad_response = HttpResponseBadRequest()#setup bad request
    bad_response['Content-Type'] = 'text/plain'#set as plain text

    if (request.method != 'POST'):# if it is not post request
        bad_response.content = "Only POST requests allowed for this resource."#bad request
        return bad_response

    logout(request)#logout using django
    flag = 0
    return HttpResponse("User has succesfully logged out.")#success


@csrf_exempt
def listRequest(request):
    global flag
    bad_response = HttpResponseBadRequest()
    bad_response['Content-Type'] = 'text/plain'

    if (flag == 0):
        bad_response.content = "You need to login in to request this."
        return bad_response

    if (request.method != 'GET'):
        bad_response.content = "Only GET requests allowed for this resource."
        return bad_response
    #module_instances = Module.objects.all().values('m_identifier', 'name', 'professor', 'year', 'semester')
##########################
    #try for module too
    all_array = []
    all_array2 = []
    all_array3 = []
    module_info = Module.objects.all() #find all professor in db
    for module in module_info:
        module_details = Module.objects.get(id = module.id)
        module_average = module_details.professor.all() #find all rating for the professor
        for prof in module_average:
            module_code = module_details.m_identifier
            module_name = module_details.name
            module_year = module_details.year
            module_semester = module_details.semester
            professor_firstname = prof.firstname
            professor_lastname = prof.lastname
            professor_id = prof.identifier
            all_prof = "{""professor_firstname"":" +  professor_firstname + "'professor_lastname':" + professor_lastname + "'professor_id':" +  professor_id
            all_array.append(str(all_prof))

        all_module = "'module_code' :" + module_code + "'module_name':" + module_name + "'module_year':" + str(module_year) +  "'module_semester' :" + str(module_semester) + "}"
        all_array.append(str(all_module))
        all_array.append("|")

    test = ""
    for i in all_array:
        if (i == "|"):
            all_array3.append(test)
            test = ""
        else:
            test = test + i
###########################
    thetest = []
    for i in all_array3:
        thetest.append(i)
    payload = {'all_info' : all_array3[1]} #prepare to return information
    http_response = HttpResponse(json.dumps(payload))
    http_response['Content-Type'] = 'application/json'
    http_response.status_code = 200
    http_response.reason_phrase = 'OK'
    return http_response

@csrf_exempt
def viewRequest(request):
    global flag
    bad_response = HttpResponseBadRequest()
    bad_response['Content-Type'] = 'text/plain'

    if (flag == 0):
        bad_response.content = "You need to login in to request this."
        return bad_response

    if (request.method != 'GET'):
        bad_response.content = "Only GET requests allowed for this resource."
        return bad_response

    average_rating = 0
    count = 0
    ratings_array = []
    professor_info = Professor.objects.all() #find all professor in db
    for prof in professor_info:
        professor = Professor.objects.get(identifier = prof.identifier)
        prof_average = professor.rating_set.all() #find all rating for the professor
        for rating in prof_average: #take an input into the array
            professor_firstname = professor.firstname
            professor_lastname = professor.lastname
            professor_id = professor.identifier
            average_rating += rating.rating
            count += 1
            average_rating2 = average_rating/count
            rounded = round(average_rating2)
        ratings_array.append({'professor_firstname': professor_firstname, 'professor_lastname': professor_lastname, 'professor_id': professor_id, 'rating': str(rounded)})
    thelist = []
    for i in ratings_array:
        thelist.append(i)
    payload = {'all_ratings' : thelist} #prepare to return information
    http_response = HttpResponse(json.dumps(payload)) #convert to json
    http_response['Content-Type'] = 'application/json' #declare as json response
    http_response.status_code = 200 #success
    http_response.reason_phrase = 'OK' #success
    return http_response



@csrf_exempt
def averageRequest(request):
    global flag
    bad_response = HttpResponseBadRequest()#setup bad request
    bad_response['Content-Type'] = 'text/plain' #set to plain text

    if (flag == 0):
        bad_response.content = "You need to login in to request this."
        return bad_response

    if (request.method != 'POST'): #if the request is not post
        bad_response.content = "Only POST requests allowed for this resource." #bad request
        return bad_response
    professor_id = request.POST['professor_id'] #take professor_id from command line client
    module_code = request.POST['module_code'] #take module_code from command line client
    check1 = Professor.objects.filter(identifier = professor_id) #filter db for the professor_id
    check2 = Module.objects.filter(m_identifier = module_code) #filter db for module_code
    if not check1: #professor_id doesnt exists
        bad_response.content = "No such professor." #bad_response
        return bad_response
    if not check2: #module_code doesnt exist
        bad_response.content = "No such module." #bad_response
        return bad_response
    #filter ratings table for certain professor and certain module
    #loop over results taking each rating and overall average
    #return average
    ratings_array = []
    professor_info = Professor.objects.get(identifier = professor_id) #find professor based on id in db
    prof_average = professor_info.rating_set.all() #find all rating for the professor
    for rating in prof_average: #loop through rating by rating
        if (rating.module.m_identifier == module_code): #if the rating is for the module provided
            ratings_array.append(rating.rating) #take an input into the array

    average_rating = 0
    count = 0
    for i in ratings_array: #for each rating in the array
        average_rating = average_rating + i #sum ratings
        count = count + 1 #keep track of amount of ratings

    if count == 0: #no ratings
        bad_response.content = "No avaliable ratings for this module." #bad response
        return bad_response
    average_rating2 = average_rating / count #work out average rating
    rounded = round(average_rating2) #round to integer

    module_details = Module.objects.filter(m_identifier = module_code).first() #find first module matching module code from db
    professor_details = Professor.objects.get(identifier = professor_id) #get professor based on id from db
    module_name = module_details.name #access module name
    professor_firstname = professor_details.firstname #access professor name
    professor_lastname = professor_details.lastname #access professor name
    #prepare to return information
    item = {'professor_id': professor_id, 'professor_firstname': professor_firstname, 'professor_lastname': professor_lastname, 'module_code': module_code, 'module_name': module_name, 'average': rounded}
    payload = {'average_rating' : item} #prepare to return information
    http_response = HttpResponse(json.dumps(payload)) #convert to json
    http_response['Content-Type'] = 'application/json' #declare as json response
    http_response.status_code = 200 #success
    http_response.reason_phrase = 'OK' #success
    return http_response

@csrf_exempt
def ratingRequest(request):
    global flag
    bad_response = HttpResponseBadRequest()#setup bad request
    bad_response['Content-Type'] = 'text/plain' #set to plain text

    if (flag == 0):
        bad_response.content = "You need to login in to request this."
        return bad_response

    if (request.method != 'POST'): #if the request is not post
        bad_response.content = "Only POST requests allowed for this resource." #bad request
        return bad_response
    professor_id = request.POST['professor_id'] #take professor_id from command line client
    module_code = request.POST['module_code'] #take module_code from command line client
    check1 = Professor.objects.filter(identifier = professor_id) #filter db for the professor_id
    check2 = Module.objects.filter(m_identifier = module_code) #filter db for module_code
    if not check1: #professor_id doesnt exists
        bad_response.content = "No such professor." #bad_response
        return bad_response
    if not check2: #module_code doesnt exist
        bad_response.content = "No such module." #bad_response
        return bad_response
