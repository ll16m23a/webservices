from django.shortcuts import render
from django.http import HttpResponse, HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt
from .models import Professor, Module, Rating, Instance
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
import json
from django.core import serializers
from decimal import Decimal, ROUND_HALF_UP

flag = 0 #login flag

@csrf_exempt
def registerRequest(request):
    bad_response = HttpResponseBadRequest() #prepare bad request
    bad_response['Content-Type'] = 'text/plain' #set to text

    if (request.method != 'POST'): #check if it a post request or not
        bad_response.content = "Only POST requests allowed for this resource." #if its get request throw error
        return bad_response

    username_validation = request.POST['username'] #take username given on command line
    username_search = User.objects.filter(username = username_validation) #check if it already exists
    if username_search: #if it exists already
        bad_response.content = "Username already taken." #already exists so bad request
        return bad_response

    email_validation = request.POST['email']#take email given on command line
    email_search = User.objects.filter(email = email_validation) #check if it already exists
    if email_search: #if it exists
        bad_response.content = "Email already in use with an account." #bad request
        return bad_response
    #otherwise create and register user and return success
    new_user = User.objects.create_user(username = request.POST['username'], password = request.POST['password'], email = request.POST['email'])
    return HttpResponse("User has succesfully registered.")

@csrf_exempt
def loginRequest(request):
    global flag
    bad_response = HttpResponseBadRequest()#setup bad request
    bad_response['Content-Type'] = 'text/plain'#set to text

    if (request.method != 'POST'):#if the request is not post
        bad_response.content = "Only POST requests allowed for this resource."#bad request
        return bad_response

    username = request.POST['username']#take username from command line client
    password = request.POST['password']#take password from command line client
    user = authenticate(username = username, password = password)#authenticate using django
    if user is not None:
        if user.is_active:
            login(request, user)#login using django
            flag = 1
            return HttpResponse("Success")#success
        else:
            bad_response.content = "User is not active."#bad request
            return bad_response
    else:
        bad_response.content = "Username/Password incorrect."#bad request
        return bad_response

@csrf_exempt
def logoutRequest(request):
    global flag
    bad_response = HttpResponseBadRequest()#setup bad request
    bad_response['Content-Type'] = 'text/plain'#set as plain text

    if (request.method != 'POST'):# if it is not post request
        bad_response.content = "Only POST requests allowed for this resource."#bad request
        return bad_response

    logout(request)#logout using django
    flag = 0
    return HttpResponse("User has succesfully logged out.")#success

@csrf_exempt
def listRequest(request):
    global flag #flag
    bad_response = HttpResponseBadRequest()#setup bad response
    bad_response['Content-Type'] = 'text/plain'#plain text format

    if (flag == 0):#if user is not logged in
        bad_response.content = "You need to login in to request this."#bad request
        return bad_response

    if (request.method != 'GET'):#if user tries to post request
        bad_response.content = "Only GET requests allowed for this resource." #bad request
        return bad_response

    all_array = [] #initialise arry for usage
    all_array2 = []
    all_array3 = []
    count = 0 #inital value for counter

    module_info = Instance.objects.all() #find all instances module in db
    for module in module_info: #for each module instance from all modules instances
        module_details = Instance.objects.get(id = module.id) #get every instance one by one using default id
        module_average = module_details.professor.all() #find all professors teaching that the module instance

        for prof in module_average: #for professor teaching that module
            module_code = module_details.module.m_identifier #save module details
            module_name = module_details.module.name
            module_year = module_details.year
            module_semester = module_details.semester
            professor_firstname = prof.firstname #save that professors details
            professor_lastname = prof.lastname
            professor_id = prof.identifier
            #setup as a dict, as more than one professor can teach a module, count is added along with the key as a dict must have unique keys
            all_prof = {'professor_firstname' + str(count): professor_firstname, 'professor_lastname'  + str(count): professor_lastname, 'professor_id' + str(count): professor_id}
            all_array.append((all_prof)) #append to array
            count = count + 1 #increment count to keep track of how many teach the module

        #setup as dict for module details once all professors are added
        all_module = {'module_code' : module_code, 'module_name': module_name, 'module_year': module_year, 'module_semester' : module_semester, 'count' : count}
        all_array.append((all_module)) #add to array
        all_array.append("|")#add | to seperate so I know where the next record begins for the next module
        count = 0 #reset count for next module

    test = {}
    for i in all_array:#for all in array
        if (i == "|"):#if the seperator is found
            all_array3.append(test)#add the dict formed so far into another array
            test = {} #reset dict formed
        else:
            test = {**test, **i} #merge dict as it is details for the professors which teach the module

    thetest = []
    for i in all_array3:
        thetest.append(i)#setup json

    payload = {'all_info' : thetest} #prepare to return information
    http_response = HttpResponse(json.dumps(payload))#return as json
    http_response['Content-Type'] = 'application/json'#set type to json
    http_response.status_code = 200#success
    http_response.reason_phrase = 'OK' #success
    return http_response

@csrf_exempt
def viewRequest(request):
    global flag
    bad_response = HttpResponseBadRequest() #setup bad response
    bad_response['Content-Type'] = 'text/plain'#set as text

    if (flag == 0):#if user is not logged in
        bad_response.content = "You need to login in to request this."# bad request
        return bad_response

    if (request.method != 'GET'):#if it is not a get request
        bad_response.content = "Only GET requests allowed for this resource."#bad request
        return bad_response

    average_rating = 0 #set as default
    count = 0 #set as default
    ratings_array = []

    professor_info = Professor.objects.all() #find all professor in db
    for prof in professor_info:#for each professor from all professors
        professor = Professor.objects.get(identifier = prof.identifier)#get professor one by one
        prof_average = professor.rating_set.all() #find all rating for the professor

        if prof_average: #if they have a rating
            for rating in prof_average: #for all ratings for that professor
                professor_firstname = professor.firstname #get relevant info
                professor_lastname = professor.lastname
                professor_id = professor.identifier
                average_rating += rating.rating #append rating to work out average
                count += 1 #keep count of ratings to work out average
                average_rating2 = average_rating/count #calculate average
                rounded = Decimal(average_rating2).quantize(0, ROUND_HALF_UP)#round correctly
            ratings_array.append({'professor_firstname': professor_firstname, 'professor_lastname': professor_lastname, 'professor_id': professor_id, 'rating': str(rounded), 'average': average_rating, 'count': count, 'average_rating2': average_rating2}) #setup as dict and add to array
            average_rating = 0
            count = 0
        thelist = []

    for i in ratings_array:
        thelist.append(i)#prepare to export

    payload = {'all_ratings' : thelist} #prepare to return information
    http_response = HttpResponse(json.dumps(payload)) #convert to json
    http_response['Content-Type'] = 'application/json' #declare as json response
    http_response.status_code = 200 #success
    http_response.reason_phrase = 'OK' #success
    return http_response

@csrf_exempt
def averageRequest(request):
    global flag
    bad_response = HttpResponseBadRequest()#setup bad request
    bad_response['Content-Type'] = 'text/plain' #set to plain text

    if (flag == 0):#if user is not logged in
        bad_response.content = "You need to login in to request this."
        return bad_response

    if (request.method != 'POST'): #if the request is not post
        bad_response.content = "Only POST requests allowed for this resource." #bad request
        return bad_response

    professor_id = request.POST['professor_id'] #take professor_id from command line client
    module_code = request.POST['module_code'] #take module_code from command line client
    check1 = Professor.objects.filter(identifier = professor_id) #filter db for the professor_id
    check2 = Module.objects.filter(m_identifier = module_code) #filter db for module_code

    if not check1: #professor_id doesnt exists
        bad_response.content = "No such professor." #bad_response
        return bad_response
    if not check2: #module_code doesnt exist
        bad_response.content = "No such module." #bad_response
        return bad_response

    #filter ratings table for certain professor and certain module
    #loop over results taking each rating and overall average
    #return average
    ratings_array = []
    professor_info = Professor.objects.get(identifier = professor_id) #find professor based on id in db
    prof_average = professor_info.rating_set.all() #find all rating for the professor
    for rating in prof_average: #loop through rating by rating
        if (rating.module.module.m_identifier == module_code): #if the rating is for the module provided
            ratings_array.append(rating.rating) #take an input into the array

    average_rating = 0
    count = 0
    for i in ratings_array: #for each rating in the array
        average_rating = average_rating + i #sum ratings
        count = count + 1 #keep track of amount of ratings

    if count == 0: #no ratings
        bad_response.content = "No avaliable ratings for this module." #bad response
        return bad_response
    average_rating2 = average_rating / count #work out average rating
    rounded = Decimal(average_rating2).quantize(0, ROUND_HALF_UP) #round to integer

    module_details = Module.objects.filter(m_identifier = module_code).first() #find first module matching module code from db
    professor_details = Professor.objects.get(identifier = professor_id) #get professor based on id from db
    module_name = module_details.name #access module name
    professor_firstname = professor_details.firstname #access professor name
    professor_lastname = professor_details.lastname #access professor name

    #prepare to return information
    item = {'professor_id': professor_id, 'professor_firstname': professor_firstname, 'professor_lastname': professor_lastname, 'module_code': module_code, 'module_name': module_name, 'average': str(rounded)}
    payload = {'average_rating' : item} #prepare to return information
    http_response = HttpResponse(json.dumps(payload)) #convert to json
    http_response['Content-Type'] = 'application/json' #declare as json response
    http_response.status_code = 200 #success
    http_response.reason_phrase = 'OK' #success
    return http_response

@csrf_exempt
def ratingRequest(request):
    global flag
    bad_response = HttpResponseBadRequest()#setup bad request
    bad_response['Content-Type'] = 'text/plain' #set to plain text

    if (flag == 0):#if user is not logged in
        bad_response.content = "You need to login in to request this."#bad request
        return bad_response

    if (request.method != 'POST'): #if the request is not post
        bad_response.content = "Only POST requests allowed for this resource." #bad request
        return bad_response

    professor_id = request.POST['professor_id'] #take professor_id from command line client
    module_code = request.POST['module_code'] #take module_code from command line client
    year = request.POST['year'] #take year from command line client
    semester = request.POST['semester'] #take semester from command line client
    rating = request.POST['rating'] #take rating from command line client

    check3 = Professor.objects.filter(identifier = professor_id) #filter db for the professor_id
    check4 = Module.objects.filter(m_identifier = module_code) #filter db for module_code

    if not check3: #professor_id doesnt exists
        bad_response.content = "No such professor." #bad_response
        return bad_response
    if not check4: #module_code doesnt exist
        bad_response.content = "No such module." #bad_response
        return bad_response

    check1 = Professor.objects.get(identifier = professor_id) #get professor from db for the professor_id
    check2 = Module.objects.get(m_identifier = module_code) #get module from db for module_code
    check_foriegn = Instance.objects.get(module = check2, year = year, semester = semester, professor = check1) #get module instance from db which matches the module id they requested
    if check_foriegn:
        if(check_foriegn.year != int(year)):#if the year doesnt match the input year for module provided
            bad_response.content = "Wrong year for module."  #bad_response
            return bad_response
        if(check_foriegn.semester != int(semester)):#if the semester doesnt match the input semester for module provided
            bad_response.content = "Wrong semester for module." #bad_response
            return bad_response
        if (int(rating) < 1): #if rating provided is not valid
            bad_response.content = "Rating's can only be between 1-5." #bad_response
            return bad_response
        if (int(rating) > 5): #if rating provided is not valid
            bad_response.content = "Rating's can only be between 1-5."#bad_response
            return bad_response

        var = check_foriegn.professor.all() #return all professors teaching the module code provided
        for professor in var:   #for each professor from all teaching the module
            if(str(professor.identifier) == str(professor_id)): #if the professor provided matches the professor actually teaching the module
                save_rating = Rating(professor = check1, module = check_foriegn, rating = rating)#save rating to db
                save_rating.save()#save
                return HttpResponse("Successfully rated the professor.")#success
    else:
        bad_response.content = "No such module instance."#otherwise bad_response
        return bad_response
    bad_response.content = "This professor doesn't teach this module."#otherwise bad_response
    return bad_response
